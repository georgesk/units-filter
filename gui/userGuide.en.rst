Units-Master User Guide
#######################

units-master is a program that allows one to enter an expression of a
physical or chemical quantity, for example 1.8 km/h or 3.2 µmol/L, and
make a few operations with it: check the expression's syntax, perform
conversions, for example convert 1.8 km/h to meters per second, which
yields 0.5 m/s, and render the expression to various formats: (plain)
LaTeX, PNG and SVG.

First: enter a quantity
=======================

In the "INPUT" section, enter some quantity: generally a numeric value
followed by a physical or chemical unit. Expressions like "1.8 km/h" or
"5h 5min 32s" are allowed; when units are multiplied, use a dot, like in
the torque unit "N.m"; accelerations can be expressed with a unit like
"m.s^-2", or "m/s/s".

Check the syntax
================

The button below the input line allows you you check your expression.
If your expression is correct, the line below will display its expression
as a polynomial of base SI units. For example, "1 N" should yield
"1 m.kg.s^-2", since a force is the product of a mass by an acceleration.

If there is some error, the same line will provide a message related to it.

Convert to ...
==============

The combo-box labeled "Unit" asks you for a new unit. For example, if you
type (and validate) "kN", and if the previous expression was "1000 N",
it should be replaced by "1.OOO kN", and you should get an success message
below; If the requested unit is not compatible with the input's unit, and
error message will be displayed below.

Render an expression
====================

The menu at the bottom right part of the window lets you choose a render
format. Currently supported formats are LaTeX, PNG and PDF. The rendering
begins when you modify the format's choice.
