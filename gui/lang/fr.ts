<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr_FR">
<context>
    <name>About</name>
    <message>
        <location filename="../ui_about.py" line="50"/>
        <source>About Units-master</source>
        <translation>À propos de UNits-Master</translation>
    </message>
</context>
<context>
    <name>MainMaster</name>
    <message>
        <location filename="../master" line="116"/>
        <source>Save PNG</source>
        <translation>Enregistrer l'image (PNG)</translation>
    </message>
    <message>
        <location filename="../master" line="127"/>
        <source>Save SVG</source>
        <translation>Enregistrer l'image (SVG)</translation>
    </message>
    <message>
        <location filename="../master" line="152"/>
        <source>Choose a PNG file name</source>
        <translation>Choisissez un nom de fichier PNG</translation>
    </message>
    <message>
        <location filename="../master" line="152"/>
        <source>PNG Images (*.png)</source>
        <translation>Images PNG (*.png)</translation>
    </message>
    <message>
        <location filename="../master" line="164"/>
        <source>Choose a SVG file name</source>
        <translation>Choisissez un nom de fichier SVG</translation>
    </message>
    <message>
        <location filename="../master" line="164"/>
        <source>SVG Scalable Images (*.svg)</source>
        <translation>Images vectorielles SVG (*.svg)</translation>
    </message>
    <message>
        <location filename="../master" line="255"/>
        <source>Bad unit: {unit}</source>
        <translation>Unité incorrecte : {unit}</translation>
    </message>
    <message>
        <location filename="../master" line="266"/>
        <source>Wanted unit {unit} does not match {baseUnit}</source>
        <translation>L'unité demandée {unit} n'est pas compatible avec {baseUnit}</translation>
    </message>
    <message>
        <location filename="../master" line="270"/>
        <source>Sucessfully converted to {unit}</source>
        <translation>Conversion réussie vers {unit}</translation>
    </message>
    <message>
        <location filename="../master" line="322"/>
        <source>Empty expression</source>
        <translation>Expression vide</translation>
    </message>
    <message>
        <location filename="../master" line="357"/>
        <source>???</source>
        <translation>???</translation>
    </message>
    <message>
        <location filename="../master" line="363"/>
        <source>Error at {place}</source>
        <translation>Erreur au caractère {place}</translation>
    </message>
    <message>
        <location filename="../master" line="369"/>
        <source>Syntax is correct</source>
        <translation>La syntaxe est correcte</translation>
    </message>
    <message>
        <location filename="../master" line="395"/>
        <source>(dimensionless)</source>
        <translation>(sans unité)</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../ui_main.py" line="143"/>
        <source>MainWindow</source>
        <translation type="obsolete">MainWindow</translation>
    </message>
    <message>
        <location filename="../ui_main.py" line="147"/>
        <source>Type a physical/chemical value, with its unit</source>
        <translation>Taper une grandeur physue ou chimlque, avec son unité</translation>
    </message>
    <message>
        <location filename="../ui_main.py" line="145"/>
        <source>INPUT</source>
        <translation>ENTRÉE</translation>
    </message>
    <message>
        <location filename="../ui_main.py" line="146"/>
        <source>Value: </source>
        <translation>Valeur : </translation>
    </message>
    <message>
        <location filename="../ui_main.py" line="148"/>
        <source>1.25 m/s</source>
        <translation>1.25 m/s</translation>
    </message>
    <message>
        <location filename="../ui_main.py" line="149"/>
        <source>Check the syntax</source>
        <translation>Vérifiez la syntaxe</translation>
    </message>
    <message>
        <location filename="../ui_main.py" line="150"/>
        <source>Click here to check the input&apos;s expression</source>
        <translation>Cliquez ici pour vérifier l'expression de l'entrée</translation>
    </message>
    <message>
        <location filename="../ui_main.py" line="151"/>
        <source>???</source>
        <translation>???</translation>
    </message>
    <message>
        <location filename="../ui_main.py" line="152"/>
        <source>Click above to check</source>
        <translation>Cliquez ci-dessus pour vérification</translation>
    </message>
    <message>
        <location filename="../ui_main.py" line="153"/>
        <source>Convert to ...</source>
        <translation>Convertir en ...</translation>
    </message>
    <message>
        <location filename="../ui_main.py" line="154"/>
        <source>Unit: </source>
        <translation>Unité : </translation>
    </message>
    <message>
        <location filename="../ui_main.py" line="155"/>
        <source>type a unit above</source>
        <translation>tapez une unité ci-dessus</translation>
    </message>
    <message>
        <location filename="../ui_main.py" line="156"/>
        <source>Chose a unit, which is compatible with the input&apos;s expression</source>
        <translation>Choisissez une unité, qui soit compatible avec l'expression à l'entrée</translation>
    </message>
    <message>
        <location filename="../ui_main.py" line="157"/>
        <source>new unit</source>
        <translation>nouvelle unité</translation>
    </message>
    <message>
        <location filename="../ui_main.py" line="158"/>
        <source>Message: </source>
        <translation>Message : </translation>
    </message>
    <message>
        <location filename="../ui_main.py" line="159"/>
        <source>Render</source>
        <translation>Rendu</translation>
    </message>
    <message>
        <location filename="../ui_main.py" line="160"/>
        <source>Choose a render format</source>
        <translation>Choisissez un format de rendu</translation>
    </message>
    <message>
        <location filename="../ui_main.py" line="161"/>
        <source>None (choose a format)</source>
        <translation>Aucun (choisissez un format)</translation>
    </message>
    <message>
        <location filename="../ui_main.py" line="162"/>
        <source>LaTeX</source>
        <translation>LaTeX</translation>
    </message>
    <message>
        <location filename="../ui_main.py" line="163"/>
        <source>PNG</source>
        <translation>PNG</translation>
    </message>
    <message>
        <location filename="../ui_main.py" line="164"/>
        <source>SVG</source>
        <translation>SVG</translation>
    </message>
    <message>
        <location filename="../ui_main.py" line="165"/>
        <source>Help</source>
        <translation>Aide</translation>
    </message>
    <message>
        <location filename="../ui_main.py" line="166"/>
        <source>About</source>
        <translation>À propos</translation>
    </message>
    <message>
        <location filename="../ui_main.py" line="167"/>
        <source>User Guide</source>
        <translation>Guide de l'utilisateur</translation>
    </message>
    <message>
        <location filename="../ui_main.py" line="168"/>
        <source>Quit</source>
        <translation>Quitter</translation>
    </message>
    <message>
        <location filename="../ui_main.py" line="143"/>
        <source>Units-Master</source>
        <translation>Units-Master</translation>
    </message>
</context>
<context>
    <name>UnitsMaster</name>
    <message>
        <location filename="../main.ui" line="14"/>
        <source>Units-Master</source>
        <translation type="obsolete">Units-Master</translation>
    </message>
    <message>
        <location filename="../main.ui" line="50"/>
        <source>Type a physical/chemical value, with its unit</source>
        <translation type="obsolete">Taper une grandeur physique ou chimlque, avec son unité</translation>
    </message>
    <message>
        <location filename="../main.ui" line="37"/>
        <source>INPUT</source>
        <translation type="obsolete">ENTRÉE</translation>
    </message>
    <message>
        <location filename="../main.ui" line="43"/>
        <source>Value: </source>
        <translation type="obsolete">Valeur : </translation>
    </message>
    <message>
        <location filename="../main.ui" line="53"/>
        <source>1.25 m/s</source>
        <translation type="obsolete">1.25 m/s</translation>
    </message>
    <message>
        <location filename="../main.ui" line="63"/>
        <source>Check the syntax</source>
        <translation type="obsolete">Vérifiez la syntaxe</translation>
    </message>
    <message>
        <location filename="../main.ui" line="69"/>
        <source>Click here to check the input&apos;s expression</source>
        <translation type="obsolete">Cliquez ici pour vérifier l'expression de l'entrée</translation>
    </message>
    <message>
        <location filename="../main.ui" line="72"/>
        <source>???</source>
        <translation type="obsolete">???</translation>
    </message>
    <message>
        <location filename="../main.ui" line="89"/>
        <source>Click above to check</source>
        <translation type="obsolete">Cliquez ci-dessus pour vérification</translation>
    </message>
    <message>
        <location filename="../main.ui" line="102"/>
        <source>Convert to ...</source>
        <translation type="obsolete">Converti en ...</translation>
    </message>
    <message>
        <location filename="../main.ui" line="108"/>
        <source>Unit: </source>
        <translation type="obsolete">UNité : </translation>
    </message>
    <message>
        <location filename="../main.ui" line="121"/>
        <source>type a unit above</source>
        <translation type="obsolete">tapez une unité ci-dessus</translation>
    </message>
    <message>
        <location filename="../main.ui" line="131"/>
        <source>Chose a unit, which is compatible with the input&apos;s expression</source>
        <translation type="obsolete">Choisissez une unité, qui soit compatible avec l'expression à l'entrée</translation>
    </message>
    <message>
        <location filename="../main.ui" line="137"/>
        <source>new unit</source>
        <translation type="obsolete">nouvelle unité</translation>
    </message>
    <message>
        <location filename="../main.ui" line="147"/>
        <source>Message: </source>
        <translation type="obsolete">Message : </translation>
    </message>
    <message>
        <location filename="../main.ui" line="169"/>
        <source>Render</source>
        <translation type="obsolete">Rendu</translation>
    </message>
    <message>
        <location filename="../main.ui" line="213"/>
        <source>Choose a render format</source>
        <translation type="obsolete">Choisissez un format de rendu</translation>
    </message>
    <message>
        <location filename="../main.ui" line="217"/>
        <source>None (choose a format)</source>
        <translation type="obsolete">Aucun (choisissez un format)</translation>
    </message>
    <message>
        <location filename="../main.ui" line="222"/>
        <source>LaTeX</source>
        <translation type="obsolete">LaTeX</translation>
    </message>
    <message>
        <location filename="../main.ui" line="227"/>
        <source>PNG</source>
        <translation type="obsolete">PNG</translation>
    </message>
    <message>
        <location filename="../main.ui" line="232"/>
        <source>SVG</source>
        <translation type="obsolete">SVG</translation>
    </message>
    <message>
        <location filename="../main.ui" line="256"/>
        <source>Help</source>
        <translation type="obsolete">Aide</translation>
    </message>
    <message>
        <location filename="../main.ui" line="268"/>
        <source>About</source>
        <translation type="obsolete">À propos</translation>
    </message>
    <message>
        <location filename="../main.ui" line="273"/>
        <source>User Guide</source>
        <translation type="obsolete">Guide de l'utilisateur</translation>
    </message>
    <message>
        <location filename="../main.ui" line="278"/>
        <source>Quit</source>
        <translation type="obsolete">Quitter</translation>
    </message>
</context>
<context>
    <name>UserGuide</name>
    <message>
        <location filename="../ui_userGuide.py" line="50"/>
        <source>Unit-Master User Guide</source>
        <translation>Guide de l'utilisateur de Units-Master</translation>
    </message>
</context>
</TS>
