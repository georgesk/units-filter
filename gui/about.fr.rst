Units-Master
############

Units-master est un programme graphique qui permet de gérer des expressions de
quantités physique et chimiques.

Auteur
======

* © 2021 Georges Khaznadar <georgesk@debian.org>

Licence
=======

Units-master est un logiciel libre : on peut le copier, examiner sa source,
le modifier et le distribuer dans sa forme originale ou après modification.
La licence complète est
GNU General Public License, version 3.

Avec un système Debian, on peut trouver cette licence à
`/usr/share/common-licenses/GPL-3 <file:///usr/share/common-licenses/GPL-3>`_.

