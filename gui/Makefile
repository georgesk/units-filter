DESTDIR =
UI_FILES = $(shell ls *.ui)
RC_FILES = $(shell ls *.qrc)
RST_HELPFILES = $(shell ls userGuide.*.rst about.*.rst)

UI_PY = $(patsubst %.ui, ui_%.py, $(UI_FILES))
RC_PY = $(patsubst %.qrc, %_rc.py, $(RC_FILES))

HTML_HELPFILES = $(patsubst %.rst,%.html, $(RST_HELPFILES))

SUBDIRS = lang

all: $(UI_PY) $(RC_PY) $(HTML_HELPFILES)
	for d in $(SUBDIRS); do \
	  $(MAKE) -C $$d $@ DESTDIR=$(DESTDIR); \
	done
	cat units-master.1 | gzip -c9 > units-master.1.gz

ui_%.py: %.ui
	pyuic5 -o $@ $<

%_rc.py: %.qrc
	pyrcc5 -o $@ $<

%.html: %.rst
	rst2html $< > $@

clean:
	rm -f *~
	rm -f $(UI_PY) $(RC_PY) $(HTML_HELPFILES)
	for d in $(SUBDIRS); do \
	  $(MAKE) -C $$d $@ DESTDIR=$(DESTDIR); \
	done
	rm -f units-master.1.gz

install: all
	# install master, other Python files, and html documents
	install -d $(DESTDIR)/usr/share/units-filter
	install -m 755 master $(DESTDIR)/usr/share/units-filter/
	install -m 644 *.py $(DESTDIR)/usr/share/units-filter/
	install -m 644 $(HTML_HELPFILES) $(DESTDIR)/usr/share/units-filter/
	# install the manpage
	install -d $(DESTDIR)/usr/share/man/man1
	install -m 644 units-master.1.gz $(DESTDIR)/usr/share/man/man1
	# install units-master
	install -d $(DESTDIR)/usr/bin
	install -m 755 units-master $(DESTDIR)/usr/bin/
	# install the logo
	install -d $(DESTDIR)/usr/share/icons/hicolor/scalable/apps/
	install -m 644 units.svg $(DESTDIR)/usr/share/icons/hicolor/scalable/apps/
	# and finally, the desktop file
	install -d $(DESTDIR)/usr/share/applications
	install -m 644 units-master.desktop $(DESTDIR)/usr/share/applications
	for d in $(SUBDIRS); do \
	  $(MAKE) -C $$d $@ DESTDIR=$(DESTDIR); \
	done

.PHONY: all clean install
