UNITS-MASTER
############

This is a graphic user interface which uses the features of
`units-filter` and of `LaTeX`

Copyright
=========

This applications is © 2021 Georges Khaznadar <georgesk@debian.org>
License: GPL-3+

It uses some images coming from /usr/share/icons/oxygen/base/32x32/actions
These images are authored by various authors: <kde-frameworks-devel@kde.org>
Copyrights are from 2007, license is LGPL-3+
